#! /usr/bin/env python3

#Created on 4 de nov. 2019
############################################################################
#   Copyright 2019 Pere Nubiola
#
#   This file is part of SeguimentProcessos.
#
#   SeguimentProcessos is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   SeguimentProcessos is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with SeguimentProcessos.  If not, see <https://www.gnu.org/licenses/>
#   @author: Pere Nubiola Radigales
############################################################################
import getopt,sys
import os
import os.path
import subprocess
import datetime
import gi
from numpy.core.defchararray import upper, title
from gi.overrides.Gtk import TreePath
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from pathlib import Path

class MaxCpuProcess(Gtk.Window):
    def __init__(self,pid):
        self.preserveCM = []
        self.preservePCM = []
        self.preserveArgs = {}
        self.initFunc = True
        self.dirpath = os.path.expanduser("~/.seguimentprocessos")
        if os.path.exists(self.dirpath):
            path = self.dirpath + "/seguimentprocessos.log"
            if os.path.exists(path):
                if os.path.getsize(path) > (1024 * 1024):
                    c = 1 
                    while os.path.exists(path + str(c)):
                        if c > 4:
                            os.remove(path + str(c))
                            break
                        c += 1
                    for i in range(c , 1 , -1):
                        os.rename(path + str(i - 1), path + str(i))
                    os.rename(path , path + str(1))
            self.path = path
        else:
            os.mkdir(self.dirpath)
            self.path = self.dirpath + "/seguimentprocessos.log"
        
        exePath = os.path.dirname(os.path.realpath(__file__))
        
        self.logoFile = os.path.join (exePath , "image/SeguimentProcessos82x64.png") 

        Gtk.Window.__init__(self, title="Control of the employment of Cpu")
        tit = "This program must run some software as root"
        self.useSudo = False
        for i in range(3):
            self.UP = self.getRootPass( tit , "Password")
            if self.UP is None:
                self.initFunc = False
                return
            if self.reniceCommand(-20, pid) == 0:
                break
            lines = ["InvalidPassword"] + self.commandToList()
            self.write(lines)
            self.UP = None 
            tit = "Invalid Password"
        if self.UP is None:
            return

        self.set_border_width(10)
        
        #Setting up the self.grid in which the elements are to be positionned
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        
        #Creating the ListStore model
        self.readConfig()
        self.columnProces()
        self.createProcList()
        self.psaCommand()
        
        #creating the treeview,  and adding the columns
        self.treeview = Gtk.TreeView.new_with_model(self.procListstore)
        self.treeview.get_selection().set_mode(Gtk.SelectionMode.SINGLE)
        for i, column_title in enumerate(self.header):
            renderer = Gtk.CellRendererText()
            if column_title[1] == "r":
                renderer.set_alignment(1.0,0.5)
            else:
                renderer.set_alignment(0.0,0.5)
            column = Gtk.TreeViewColumn(column_title[0], renderer, text=i)
            self.treeview.append_column(column)
            if i == 19:
                break
        
        iterf = self.procListstore.get_iter_first()
        if (iterf is not None):
            self.treeview.set_cursor(self.procListstore.get_path(iterf))
                
        #creating buttons to filter by programming language, and setting up their events
        self.buttons = list()
        for cmd in ["SigTerm","SigQuit","SigKill","PreserveCMD","PreserveParent","PreserveArgument"]:
            button = Gtk.Button(label = cmd)
            button.set_name(str(upper(cmd))) 
            self.buttons.append(button)
            button.connect("clicked", self.buttonClicked)
        #setting up the layout, putting the treeview in a scrollwindow, and the buttons in a row
        self.scrollableTreelist = Gtk.ScrolledWindow()
        self.scrollableTreelist.set_vexpand(True)
        self.scrollableTreelist.add(self.treeview)
        self.grid.attach(self.scrollableTreelist, 0, 0, 7, 13)

        self.grid.attach_next_to(self.buttons[0], self.scrollableTreelist, Gtk.PositionType.BOTTOM, 1, 1)
        for i, button in enumerate(self.buttons[1:]):
            self.grid.attach_next_to(button, self.buttons[i], Gtk.PositionType.RIGHT, 1, 1)
            
        self.bar = Gtk.Statusbar();
        self.bar2 = Gtk.Statusbar();
        
        self.grid.attach_next_to(self.bar,self.buttons[0], Gtk.PositionType.BOTTOM, 3, 1)
        self.grid.attach_next_to(self.bar2,self.bar, Gtk.PositionType.RIGHT, 4, 1)
        
        self.ctx1 = self.bar.get_context_id("renice")
        self.ctx = self.bar.get_context_id("timeout")
        self.ctx2 = self.bar2.get_context_id("errors")
        
        vbox = Gtk.VBox(homogeneous = False , spacing = 2)

        # Toolbar
        toolbar = Gtk.Toolbar()

        bar_item2 = Gtk.ToolButton()
        bar_item2.set_icon_name("applications-system")
        bar_item2.connect('clicked', self.onHelpButton, "as")
        toolbar.insert(bar_item2,-1)

        bar_item1 = Gtk.ToolButton()
        bar_item1.set_icon_name("help-about")
        bar_item1.connect('clicked', self.onHelpButton, "ha")
        toolbar.insert(bar_item1, -1)
        
        bar_item = Gtk.ToolButton()
        bar_item.set_icon_name("help-contents")
        bar_item.connect('clicked', self.onHelpButton, "hc")
        toolbar.insert(bar_item, -1)

        vbox.pack_start(toolbar,False,False,0)
        vbox.pack_start(self.grid,False,False,0)

        self.add(vbox)

        
        self.bar.push(self.ctx1,str(self.reniceout))
        
        self.timeoutID = -1
        self.changeTimeOut(pid)
        self.show_all()
        self.counts = 0
        self.initFunc = False
    def onHelpButton(self,toolbutton,action):
        if action == "ha":
            about = Gtk.AboutDialog()
            name = Path(__file__).stem
            about.set_program_name(name)
            about.set_version(version)
            about.set_authors(["Pere Nubiola<pere@nubiola.cat>"])
            about.set_copyright("(c) Pere Nubiola 2019")
            about.set_comments("Control of CPU occupation for processes.\n")
            about.set_license_type(Gtk.License.GPL_3_0)
            about.set_website("https://gitlab.com/pnubiola/seguimentprocessos")
            pb = GdkPixbuf.Pixbuf.new_from_file(self.logoFile)
            about.set_logo(pb)
            about.run()
            about.destroy()
        elif action == "as":
            self.windowConfig()
        return

        return
        
    def changeTimeOut(self,pid = None):
        if pid is not None:
            self.pid = pid
        if self.timeoutID != -1:
            GLib.source_remove(self.timeoutID)
        self.timeoutID = GLib.timeout_add_seconds(MaxCpuProcess.timeoutSeconds,self.psaCommand , self.pid)                
    def command(self,cmd):
        if self.useSudo :
            proc = subprocess.Popen(cmd , stdin = subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        else:
            proc = subprocess.Popen(cmd , stdin = subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = proc.communicate(self.UP.encode())
        if out[0] is not None:
            self.out = out[0].decode('utf-8')
        else:
            self.out = None;
        if len(out) > 1 and  out[1] is not None:
                self.err = out[1].decode('utf-8')
        else:
            self.err = None
        return proc.returncode        
    def reniceCommand(self,prior, pid):
        if self.useSudo :
            cmd = ["sudo","-k","-S", "su","-c", "renice " + str(prior) + " " + str(pid)]
        else:
            cmd = ["su","-c","renice " + str(prior) + " " + str(pid)]
        ret = self.command(cmd)
        self.reniceout = self.out
        self.reniceerr = self.err
        return ret
    def getCommandPid(self,pid = None):
        if pid == None:
            return None
        argcmd = "ps --no-headers -o comm --pid " + str(pid)
        if self.useSudo :
            cmd = ["sudo","-k","-S","su","-c", argcmd]
        else:
            cmd = ["su","-c", argcmd ]
        if self.command(cmd) == 0:
            return self.commandToList()[0]
        return None
    def psPPidsPreserve(self):
        argcmd = "ps --no-headers -o pid -C " + ",".join(self.preservePCM)
        if self.useSudo :
            cmd = ["sudo","-k","-S","su","-c", argcmd]
        else:
            cmd = ["su","-c", argcmd ]
        if self.command(cmd) == 0:
            return self.commandToList()
        return []
    def psArgsPreserve(self,pid):
        argcmd = "ps --no-headers -o args --pid " + str(pid)
        if self.useSudo :
            cmd = ["sudo","-k","-S","su","-c", argcmd]
        else:
            cmd = ["su","-c", argcmd ]
        if self.command(cmd) == 0:
            try:
                return  self.out.split(None,1)[1]
            except IndexError:
                return ""
        return ""
    def psCommand(self, pid = 0, arg = False):
        if pid == 0:
            argcmd = "-Ao"
        else:
            argcmd = "--pid " + str(pid) + " -o"
        cn =  self.cmd
        if arg and not "args" in cn:
            cn += ",args"
        if self.useSudo :
            cmd = ["sudo","-k","-S","su","-c", "ps " + argcmd + " " + cn + " --sort=-%cpu"]
        else:
            cmd = ["su","-c","ps " + argcmd + " " + cn + " --sort=-%cpu"]
        return self.command(cmd)
    def commandToList(self , top = None):
        if self.out.find("\n") > 0:
            chf = "\n"
        else:
            chf = "\\n"
        if top is None:
            return self.out.split(chf)
        return self.out.split(chf,top)
    def processLine(self,data):
        c = self.cmd.split(",")
        star = 0
        retl = []
        for i,d in enumerate(c):
            e = d.split(":")
            stop = star + int(e[1])
            f = data[star:stop].strip()
            if i < self.maxDisplayCommands:
                retl.append(f)
            if i == self.psCpuaCol:
                wcpu = f
            if i == self.psPidCol:
                wpid = f
            if i == self.psPidCol:
                wpid = f
            if i == self.psPPidCol:
                wppid = f
            if i == self.psNiCol:
                wnice = f
            if i == self.psCommCol:
                wcomm = f
            star = stop + 1
        return [retl,wcpu,wpid,wppid,wnice,wcomm]
    def psaCommand(self, pid = 0):
        ret = self.psCommand()
        psppid=None
        if ret == 0:
            lst = self.commandToList(14)
            self.procListstore.clear()
            self.midata={}
            for i in range(1,min(13,len(lst)-1)):
                r = self.processLine(lst[i])
                l = r[0]
                try:
                    itern = self.procListstore.append(l)
                    path = self.procListstore.get_path(itern).to_string()
                    #store the most important data for every line
                    self.midata[path]= r[1:]
                    comm = r[5]
                    nice = int(r[4])
                    ppid = int(r[3])
                    pid = int(r[2])
                    if float(r[1]) > MaxCpuProcess.perCentCPU:
                        lines = ["Process pcpu exceds 60", lst[i]]
                        if nice < 16:
                            nice += 5
                            self.reniceCommand(nice, pid)
                        elif nice < 19:
                            nice += 1
                            self.reniceCommand(nice, pid)
                        elif comm not in self.preserveCM:
                            kill = True
                            if comm in self.preserveArgs:
                                arg = self.psArgsPreserve(pid)
                                if arg != "":
                                    for a in self.preserveArgs[comm]:
                                        if a in arg:
                                            kill = False
                            if kill and psppid == None:
                                psppid = self.psPPidsPreserve()
                            if kill and ppid not in psppid:
                                self.killCommand("SIGKILL", pid)
                        if ppid != 0:
                            r1 = self.psCommand(ppid,True)
                            lst2 = self.commandToList(14)
                            if r1 == 0:
                                lines.append(lst2[1])
                            else:
                                lines += lst2
                        self.write(lines)
                except ValueError:
                    self.write(["Error append row",lst[i]])
            if not self.initFunc:
                self.treeview.set_model(self.procListstore)
                self.counts += 1
                self.bar.push(self.ctx,str(pid) + " timeout nro " + str(self.counts))
                iterf = self.procListstore.get_iter_first()
                if (iterf is not None):
                    self.treeview.set_cursor(self.procListstore.get_path(iterf))
                return True
        else:
            self.bar2.push(self.ctx2,self.err)
        return ret
    def killCommand(self, signal, pid):
        if self.useSudo :
            cmd = ["sudo","-k","kill -" + signal + " " + str(pid)]
        else:
            cmd = ["su","-c","kill -" + signal + " " + str(pid)]
        return self.command(cmd)
        
    def buttonClicked(self,widget):
        signal = widget.get_name()
        tsel = self.treeview.get_selection()
        (mod,iterr) = tsel.get_selected()
        path = mod.get_path(iterr).to_string()
        pid = self.midata[path][1]
        ppid = self.midata[path][2]
        comm = self.midata[path][4]
        if signal.lower() == "preservecmd":
            if comm not in (self.preserveCM):
                self.preserveCM.append(comm)
                self.writeCF("psPreserved=" + comm)
        elif signal.lower() == "preserveparent":
            l = self.getCommandPid(ppid)
            if l != None and l[0] not in self.preservePCM:
                self.preservePCM.append(l[0])
                self.writeCF("psParentPreserved=" + l[0])
        else:
            self.killCommand(signal, pid)
        
    def write(self,lines):
        f = open(self.path, "a+")
        f.write( datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\n")
        if isinstance(lines, list):
            for i in lines:
                f.write(i + "\n")
        else:
            f.write(lines + "\n")
        f.close()
        
    def writeCF(self,lines):
        f = open(self.dirpath + ("/seguimentprocessos.cfg"), "a+")
        if isinstance(lines, list):
            for i in lines:
                f.write(i + "\n")
        else:
            f.write(lines + "\n")
        f.close()
    def messageError(self,error, tit = "Error"):
        dialogWindow = Gtk.MessageDialog(parent = self,
            modal = True, destroy_with_parent = True,
            message_type = Gtk.MessageType.ERROR,
            buttons = Gtk.ButtonsType.OK, text = error)
        dialogWindow.set_title(tit)
        dialogWindow.show_all()
        dialogWindow.run()
        dialogWindow.destroy()
        
    def getRootPass(self, message, title=''):
        dialogWindow = Gtk.MessageDialog(parent = self,
            modal = True, destroy_with_parent = True,
            message_type = Gtk.MessageType.QUESTION,
            buttons = Gtk.ButtonsType.OK_CANCEL, text = message)
        
        dialogWindow.set_title(title)
        
        dialogBox = dialogWindow.get_content_area()
        userEntry = Gtk.Entry()
        userEntry.set_visibility(False)
        userEntry.set_invisible_char("*")
        userEntry.set_size_request(250,0)
        btnSudo = Gtk.CheckButton(label = "use sudo")
        btnSudo.set_active(self.useSudo)
        dialogBox.pack_end(btnSudo, False, False, 0)
        dialogBox.pack_end(userEntry, False, False, 0)

        dialogWindow.show_all()
        response = dialogWindow.run()
        text = userEntry.get_text()
        self.useSudo = btnSudo.get_active()
        dialogWindow.destroy()
        if response == Gtk.ResponseType.OK:
            return text
        else:
            return None
    def columnProces(self):
        l = MaxCpuProcess.psColumsName.split(",")
        top = len(l)
        self.header=[]
        self.cmd = ""
        if "pid" not in l:
            l.append("pid")
        if "ppid" not in l:
            l.append("ppid")
        if "%cpu" not in l:
            l.append("%cpu")
        if "ni" not in l:
            l.append("ni")
        if "comm" not in l:
            l.append("comm")
        i = 0
        k = 0
        for j,c in enumerate(l):
            wc = c.strip()
            if MaxCpuProcess.psTable.get(wc) is not None:
                self.cmd += wc + ":" + str(MaxCpuProcess.psTable[wc][2]) + ","
                if j < top:
                    self.header.append([MaxCpuProcess.psTable[wc][1],MaxCpuProcess.psTable[wc][3]])
                    k += 1
                if wc == "pid":
                    self.psPidCol = i
                elif wc == "ppid":
                    self.psPPidCol = i
                elif wc == "%cpu":
                    self.psCpuaCol = i
                elif wc == "ni":
                    self.psNiCol = i
                elif wc == "comm":
                    self.psCommCol = i
                i += 1
        self.maxDisplayCommands = k
        self.cmd = self.cmd[:len(self.cmd) - 1]
    def readConfig(self):
        self.defaults()
        path = self.dirpath + ("/seguimentprocessos.cfg")
        if os._exists(path):
            f = open(path, "r")
            ls = f.readlines()
            for l in ls:
                if l[:1] == "#":
                    continue
                c=l.split("=",2)
                if c[0] == "timeoutSeconds":
                    MaxCpuProcess.timeoutSeconds = int(c[1])
                elif c[0] ==  "psPerCentCPU":
                    MaxCpuProcess.perCentCPU = float(c[1])
                elif c[0] == "psColumsName":
                    MaxCpuProcess.psColumsName
                elif c[0] == "psPreserved":
                    self.preserveCM.append(c[1])
                elif c[0] == "psParentPreserved":
                    self.preservePCM.append(c[1])
                elif c[0] == "psArgsPreserved":
                    p = c[1].split(None,1)
                    if p[0] in self.preserveArgs:
                        self.preserveArgs.get(p[0]).append(p[1])
                    else:
                        self.preserveArgs.p[0] = [p[1]]
                else:
                    if MaxCpuProcess.psTable.get(c[0]) is not None:
                        MaxCpuProcess.psTable[c[0]][1] = c[1]
            f.close()
        
    def windowConfig(self):
        dialogWindow = Gtk.Dialog(
        title = "SeguimentProcessos config",
        parent = self,
        modal = True,destroy_with_parent=True
        )
        dialogWindow.add_buttons("OK", Gtk.ResponseType.OK, "CANCEL", Gtk.ResponseType.CANCEL)
        dialogBox = dialogWindow.get_content_area()

        #ListStores create
        listcmd = Gtk.ListStore(str,str)
        listexe = Gtk.ListStore(str,str)
        
        #listStores add data 
        l = MaxCpuProcess.psColumsName.split(",")
        for k in l:
            v = MaxCpuProcess.psTable.get(k)
            if v is not None:
                v = v[1]
            listexe.append([k,v])
        for k in  MaxCpuProcess.psTable:
            if k not in l:
                v = MaxCpuProcess.psTable.get(k)
                listcmd.append([k,v[1]])

        #treeviews create
        treeviewcmd = Gtk.TreeView.new_with_model(listcmd)
        treeviewexe = Gtk.TreeView.new_with_model(listexe)
        treeviewcmd.set_border_width(10)
        treeviewexe.set_border_width(10 )
        
        #add first column treeviewcmd
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CMD", renderer, text=0)
        treeviewcmd.append_column(column)
        
        #add 2nd column editable treeviewcmd
        renderer = Gtk.CellRendererText()
        renderer.set_property("editable", True)
        renderer.connect("edited", self.cell_edited, [treeviewcmd,"cmd"])
        column = Gtk.TreeViewColumn("Title", renderer, text=1)
        treeviewcmd.append_column(column)
        
        #add first column treeviewexe
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CMD", renderer, text=0)
        treeviewexe.append_column(column)
        
        #add 2nd column editable treeviewexe
        renderer = Gtk.CellRendererText()
        renderer.set_property("editable", True)
        renderer.connect("edited", self.cell_edited, [treeviewexe,"cmd"])
        column = Gtk.TreeViewColumn("Title", renderer, text=1)
        treeviewexe.append_column(column)
        
        #Drag&drop
        self.Targets = [
            ('MY_TREE_MODEL_ROW', Gtk.TargetFlags.OTHER_APP, 0),
            ('text/plain', 0, 1),
            ('TEXT', 0, 2),
            ('STRING', 0, 3),
        ]

        treeviewcmd.enable_model_drag_source( Gdk.ModifierType.BUTTON1_MASK,self.Targets,
                                                Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
        treeviewcmd.enable_model_drag_dest(self.Targets, Gdk.DragAction.DEFAULT)
        treeviewexe.enable_model_drag_source(  Gdk.ModifierType.BUTTON1_MASK,self.Targets,
                                               Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
        treeviewexe.enable_model_drag_dest(self.Targets, Gdk.DragAction.DEFAULT)
        
        treeviewcmd.connect("drag_data_get", self.drag_data_get_data,"cmd")
        treeviewexe.connect("drag_data_get", self.drag_data_get_data,"cmd")
        treeviewcmd.connect("drag_data_received", self.drag_data_received_data,"cmd")
        treeviewexe.connect("drag_data_received", self.drag_data_received_data,"cmd")
        
        treeviewcmd.set_has_tooltip(True)
        treeviewcmd.connect('query-tooltip', self.on_query_tooltip)
        treeviewexe.set_has_tooltip(True)
        treeviewexe.connect('query-tooltip', self.on_query_tooltip)

        #Setting up the self.grid in which the elements are to be positionned
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)

        #Create a entry for timeout
        label = Gtk.Label(label ="ps execution timeout: ")
        adj = Gtk.Adjustment(
            value = float(MaxCpuProcess.timeoutSeconds), 
            lower = 1.0, 
            upper = 6000.0, 
            step_increment = 1.0, 
            page_increment = 5.0, 
            page_size = 0.0)
        spin = Gtk.SpinButton()
        spin.set_adjustment(adj)
        spin.set_wrap(True)
        spin.set_numeric(True)
        grid.attach(label, 0, 0, 1, 1)
        grid.attach_next_to(spin,label,Gtk.PositionType.RIGHT,1,1)

        label1 = Gtk.Label(label = "% CPU control")
        adj1 = Gtk.Adjustment(
            value = float(MaxCpuProcess.perCentCPU), 
            lower = 50.0, 
            upper = 100.0, 
            step_increment = 5.0, 
            page_increment = 10.0, 
            page_size = 0.0)
        spin1 = Gtk.SpinButton()
        spin1.set_adjustment(adj1)
        spin1.set_wrap(True)
        spin1.set_numeric(True)
        grid.attach_next_to(label1,spin,Gtk.PositionType.RIGHT,1,1)
        grid.attach_next_to(spin1,label1,Gtk.PositionType.RIGHT,1,1)
        
        scrollableTreelist = Gtk.ScrolledWindow()
        scrollableTreelist.set_vexpand(True)
        scrollableTreelist.set_shadow_type(type=Gtk.ShadowType.ETCHED_IN)
        scrollableTreelist.add(treeviewcmd)
        
        scrollableTreelist1 = Gtk.ScrolledWindow()
        scrollableTreelist1.set_vexpand(True)
        scrollableTreelist1.set_shadow_type(type=Gtk.ShadowType.ETCHED_IN)
        scrollableTreelist1.add(treeviewexe)
        
        vbox = Gtk.HBox(homogeneous = True)
        
        hbox = Gtk.HBox(homogeneous = True , spacing = 5)
        hbox.pack_start(scrollableTreelist,True,True,2)
        hbox.pack_start(scrollableTreelist1,True,True,2)
        nb = Gtk.Notebook()
        nb.set_tab_pos(Gtk.PositionType.BOTTOM)
        nb.append_page(hbox)
        nb.set_tab_label_text(hbox,"Columns")

        listPreserveCMD = self.getPreserveCMD(nb,"c")
        listPreservePCMD = self.getPreserveCMD(nb,"p")
        #treeviewPreserveArgs = self.getPreserveArgs(nb)

        grid.attach_next_to(nb,label,Gtk.PositionType.BOTTOM,4,13)
        
        dialogBox.pack_start(grid, True, True, 10)
        dialogWindow.show_all()
        response = dialogWindow.run()
        
        if response == Gtk.ResponseType.OK:
            if os._exists(self.dirpath + ("/seguimentprocessos.cfg")):
                os.remove(self.dirpath + ("/seguimentprocessos.cfg"))
            num = int(spin.get_value())
            if num != MaxCpuProcess.timeoutSeconds:
                MaxCpuProcess.timeoutSeconds = num
                self.changeTimeOut()
            if MaxCpuProcess.timeoutSeconds != 5:
                self.writeCF("timeoutSeconds=" + str(MaxCpuProcess.timeoutSeconds))
            cmd = ""
            its =listexe.get_iter_first()
            sep = ""
            changeList = False
            while its is not None:
                cm = listexe.get_value(its,0)
                tit = listexe.get_value(its,1)
                if MaxCpuProcess.psTable.get(cm)[1] != tit:
                    MaxCpuProcess.psTable[cm][1] = tit
                    changeList = True
                cmd += sep + cm
                sep = ","
                its = listexe.iter_next(its)
            if cmd != MaxCpuProcess.psColumsName:
                MaxCpuProcess.psColumsName = cmd
                changeList = True
            its = listcmd.get_iter_first()
            while its is not None:
                cm = listcmd.get_value(its,0)
                tit = listcmd.get_value(its,0)
                if MaxCpuProcess.psTable.get(cm)[1] != tit:
                    MaxCpuProcess.psTable[cm][1] != tit
                its = listcmd.iter_next(its)
            ml = []
            for cm in MaxCpuProcess.psTable:
                if MaxCpuProcess.psTable.get(cm)[0] != MaxCpuProcess.psTable.get(cm)[1]:
                    ml.append(cm + "=" + MaxCpuProcess.psTable.get(cm)[1])
            if len(ml)>0: 
                self.writeCF(ml)
            self.preserveCM = []
            its = listPreserveCMD.get_iter_first()
            while its is not None:
                cm = listPreserveCMD.get_value(its,0)
                self.preserveCM.append(cm)
                self.writeCF("psPreserved=" + cm)
            self.preservePCM = []
            its = listPreserveCMD.get_iter_first()
            while its is not None:
                cm = listPreserveCMD.get_value(its,0)
                self.preserveCM.append(cm)
                self.writeCF("psPreserved=" + cm)
            self.preserveArgs = {}

        if hasattr(self,"Helpscomand"):
            self.Helpscomand = None
        dialogWindow.destroy()
        
    def getPreserveCMD(self,nb,typ):
        #ListStores create
        listcmd = Gtk.ListStore(str)
        listrmv = Gtk.ListStore(str)
        if typ == "p":
            co = "pcmdo"
            cd = "pcmdd"
            for v in self.preservePCM:
                listcmd.append([v])
        else:
            co = "ucmdo"
            cd = "ucmdd"
            for v in self.preserveCM:
                listcmd.append([v])
        treeviewcmd = Gtk.TreeView.new_with_model(listcmd)
        treeviewrmv = Gtk.TreeView.new_with_model(listrmv)
        treeviewcmd.set_border_width(10)
        treeviewrmv.set_border_width(10 )
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CMD", renderer, text=0)
        treeviewrmv.append_column(column)
        renderer.set_property("editable", True)
        renderer.connect("edited", self.cell_edited, [treeviewcmd,co])
        renderer.connect("editing-started", self.cell_edited_start, [treeviewcmd,co])
        renderer.connect("editing-canceled", self.cell_edited_can, [treeviewcmd,co])
        column = Gtk.TreeViewColumn("CMD", renderer, text=0)
        treeviewcmd.append_column(column)
        treeviewcmd.enable_model_drag_source( Gdk.ModifierType.BUTTON1_MASK,self.Targets,
                                                Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
        treeviewcmd.enable_model_drag_dest(self.Targets, Gdk.DragAction.DEFAULT)
        treeviewrmv.enable_model_drag_source(  Gdk.ModifierType.BUTTON1_MASK,self.Targets,
                                               Gdk.DragAction.DEFAULT | Gdk.DragAction.MOVE)
        treeviewrmv.enable_model_drag_dest(self.Targets, Gdk.DragAction.DEFAULT)
        treeviewcmd.connect("drag_data_get", self.drag_data_get_data,co)
        treeviewrmv.connect("drag_data_get", self.drag_data_get_data,cd)
        treeviewcmd.connect("drag_data_received", self.drag_data_received_data,cd)
        treeviewrmv.connect("drag_data_received", self.drag_data_received_data,co)
        treeviewcmd.connect('button_press_event', self.popupMenu, co)
        
        hbox = Gtk.HBox(homogeneous = True , spacing = 5)

        scrollableTreelist = Gtk.ScrolledWindow()
        scrollableTreelist.set_vexpand(True)
        scrollableTreelist.set_shadow_type(type=Gtk.ShadowType.ETCHED_IN)
        scrollableTreelist.add(treeviewcmd)
        hbox.pack_start(scrollableTreelist,True,True,2)

        scrollableTreelist = Gtk.ScrolledWindow()
        scrollableTreelist.set_vexpand(True)
        scrollableTreelist.set_shadow_type(type=Gtk.ShadowType.ETCHED_IN)
        scrollableTreelist.add(treeviewrmv)
        hbox.pack_start(scrollableTreelist,True,True,2)
        
        nb.append_page(hbox)
        if typ == "p":
            nb.set_tab_label_text(hbox,"Preserve Parent Commnands")
        else:
            nb.set_tab_label_text(hbox,"Preserve Comands")
        
        return listcmd

    def getPreserveArgs(self,nb):
        self.preserveArgs = {}
    def itemMenu(self,treeview,ldata):
        treeview = ldata[0]
        udata = ldata[1]
        model = treeview.get_model()
        itere = model.append([""])
        path = model.get_path(itere)
        cm = treeview.get_column(0)
        treeview.set_cursor_on_cell(path,cm,None,True)
        return
    def popupMenu(self,treeview,event,udata):
        if event.button != 3:
            return False
        label = "Add Command"
        menu = Gtk.Menu()
        menuitem = Gtk.MenuItem(label=label)
        menuitem.connect("activate",self.itemMenu,[treeview,udata])
        menu.append(menuitem)
        menu.show_all()
        menu.popup(None,None,None,None,event.button,event.time)
        return True
    def on_query_tooltip(self,treeview, x, y, keyboardMode, tooltip):
        (isrow, x1 , y1 , model , path , itert) = treeview.get_tooltip_context(x,y,keyboardMode)
        if isrow and model is not None :
            if not hasattr(self,"Helpscomand") or self.Helpscomand is None:
                self.commandsHelp()
            cmd = model.get_value(itert,0)
            text =  self.Helpscomand.get(cmd)
            tooltip.set_text(text)
            return True
        return False
    
    def drag_data_received_data(self,treeview, context, x, y, selection,info, etime,udata):
        model = treeview.get_model()
        data = selection.get_data()
        k = data.decode('utf-8').decode(".",1)
        if k[0] != udata:
            return
        if udata == "cmd":
            v = k[1].split(",",1)
        elif udata in ["ucmdo","ucmdd","ucmdo","ucmdd"]:
            v = [k[1]]
        drop_info = treeview.get_dest_row_at_pos(x, y)
        if drop_info:
            path, position = drop_info
            iterm = model.get_iter(path)
            if (position == Gtk.TreeViewDropPosition.BEFORE or position == Gtk.TreeViewDropPosition.INTO_OR_BEFORE):
                model.insert_before(iterm, v)
            else:
                model.insert_after(iterm,v)
        else:
            model.append(v)
        if context.get_suggested_action() == Gdk.DragAction.MOVE:
            context.finish(True, True, etime)
            return

    def drag_data_get_data(self, treeview, context, selection, target_id, etime,udata):
        treeselection = treeview.get_selection()
        model, iterm = treeselection.get_selected()
        k = udata + ":"
        if udata == "cmd":
            k += model.get_value(iterm,0) + "," + model.get_value(iterm,0) 
        elif udata in ["ucmdo","ucmdd","ucmdo","ucmdd"]:
            k += model.get_value(iterm,0)
        c = selection.get_target()
        selection.set( c,8,k.encode())
        print("Sending")
        
    def cell_edited_can(self, window,ldata):
        treeview = ldata[0]
        form = treeview.get_model()
        iter = form.get_iter_from_string(self.currentEditPath)
        val = form[self.currentEditPath][0]
        if val == "":
            form.remove(iter)

    def cell_edited_start(self, window, editable, path,ldata):
        self.currentEditPath = path
        
    def cell_edited(self, window, path, text, ldata):
        treeview = ldata[0]
        form = treeview.get_model()
        if ldata[1] == "cmd":
            if text == "":
                self.messageError("Null entry not accepted", "Entry Error")
                cm = treeview.get_column(1)
                treeview.set_cursor_on_cell(Gtk.TreePath.new_from_string(path),cm,window,True)
                return
            form[path][1] = text
        else:
            err = None
            if text == "":
                err = "Entry Error"
            else:
                its = form.get_iter_first()
                while its is not None:
                    if text == form.get_value(its,0):
                        if form.get_string_from_iter(its) != path:
                            error = "Duplicated commnas"
                            break
                    its = form.iter_next(its)
            if err != None:
                self.messageError("Null entry not accepted", err)
                cm = treeview.get_column(0)
                treeview.set_cursor_on_cell(Gtk.TreePath.new_from_string(path),cm,window,True)
                return
            form[path][0] = text
    def createProcList(self):
        l = len(self.header)
        if l == 3:
            self.procListstore = Gtk.ListStore(str, str, str)
        elif l == 4:
            self.procListstore = Gtk.ListStore(str, str, str, str)
        elif l == 5:
            self.procListstore = Gtk.ListStore(str, str, str, str, str)
        elif l == 6:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str)
        elif l == 7:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str)
        elif l == 8:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str)
        elif l == 9:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str)
        elif l == 10:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str)
        elif l == 11:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str)
        elif l == 12:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 13:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 14:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 15:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 16:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 17:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 18:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 19:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)
        elif l == 20:
            self.procListstore = Gtk.ListStore(str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str, str)

    def defaults(self):
        MaxCpuProcess.timeoutSeconds = 10
        MaxCpuProcess.perCentCPU = 80.0
        MaxCpuProcess.psColumsName = "user,uid,pid,ppid,%cpu,pri,tty,ni,cmd"
        MaxCpuProcess.psColumnNameST = MaxCpuProcess.psColumsName;
        #key:[DefaulTitle,Title,ColumnLength,align]
        MaxCpuProcess.psTable = {
            "%cpu":["%CPU","%CPU", 3,"r"],
            "%mem":["%MEM","%MEM", 3,"r"],
            "cmd":["COMMAND","COMMAND",80,"l"],
            "blocked":["BLOCKED","BLOCKED",16,"r"],
            "bsdstart":["START","START",7,"r"],
            "bsdtime":["TIME","TIME",7,"r"],
            "caught":["CAUGHT","CAUGHT",16,"r"],
            "cgname":["CGNAME","CGNAME",100,"l"],
            "cgroup":["CGROUP","CGROUP",250,"r"],
            "class":["CLS","CLS",3,"l"],
            "comm":["COMMAND","COMMAND",20,"l"],
            "cputime":["TIME","TIME",11,"r"],
            "cputimes":["TIME","TIME",8,"r"],
            "drs":["DRS","DRS",7,"r"],
            "egid":["EGID","EGID",5,"r"],
            "egroup":["EGROUP","EGROUP",5,"r"],
            "etime":["ELAPSED","ELAPSED",11,"r"],
            "etimes":["ELAPSED","ELAPSED",8,"r"],
            "euid":["EUID","EUID",5,"r"],
            "euser":["EUSER","EUSER",20,"l"],
            "fgid":["FGID","FGID",5,"r"],
            "fgroup":["FGRUP","FGRUP",20,"l"],
            "fuid":["FUID","FUID",5,"r"],
            "fuser":["FUSER","FUSER",20,"l"],
            "fname":["COMMAND","COMMAND",8,"l"],
            "gid":["GID","GID",5,"r"],
            "group":["GROUP","GROUP",20,"r"],
            "ignored":["IGNORED","IGNORED",16,"r"],
            "ni":["NI","NI",3,"r"],
            "nlwp":["NLWP","NLWP",3,"l"],
            "ouid":["OWNER","OWNER",5,"l"],
            "pending":["PENDING","PENDING",16,"r"],
            "pgid":["PGID","PGID",5,"r"],
            "pid":["PID","PID",5,"r"],
            "ppid":["PPID","PPID",5,"r"],
            "pri":["PRI","PRI",3,"r"],
            "psr":["PSR","PSR",3,"r"],
            "rgid":["RGID","RGID",5,"r"],
            "rgroup":["RGRUP","RGRUP",20,"l"],
            "rss":["RSS","RSS",8,"r"],
            "rtprio":["RTPRIO","RTPRIO",3,"r"],
            "ruid":["RUID","RUID",5,"r"],
            "ruser":["RUSER","RUSER",20,"l"],
            "sgid":["SGID","SGID",5,"r"],
            "sgroup":["SGROUP","SGROUP",20,"r"],
            "suid":["SUID","SUID",5,"r"],
            "suser":["SUSER","SUSER",20,"l"],
            "tty":["TT","TT",20,"l"],
            "uid":["UID","UID",5,"r"],
            "user":["USER","USER",20,"l"],
            }
    def commandsHelp(self):
        self.Helpscomand = {
            "%cpu":"cpu utilization of the process in \"##.#\" format.  Currently,"
            " it is the CPU time used divided by the time the process has been running"
            " (cputime/realtime ratio), expressed as a percentage. It will not add up "
            "to 100% unless you are lucky.",
            "%mem":"ratio of the process's resident set size  to the physical memory on"
            " the machine, expressed as a percentage.",
            "cmd":"command with all its arguments as a string. Modifications to the "
            "arguments may be shown. The output in this column may contain spaces. A"
            " process marked <defunct> is partly dead, waiting to be fully destroyed"
            " by its parent. Sometimes the process args will be unavailable; when this"
            " happens, ps will instead print the executable name in brackets.",
            "blocked":"mask of the blocked signals, see signal(7). According to the "
            "width of the field, a 32 or64-bit mask in hexadecimal format is displayed.",
            "bsdstart":"time the command started.  If the process was started less than"
            " 24 hours ago, the output format is \" HH:MM\", else it is \" Mmm:SS\" "
            "(where Mmm is the three letters of the month).",
            "bsdtime":"accumulated cpu time, user + system.  The display format is usually"
            " \"MMM:SS\", but can be shifted to the right if the process used more than 999"
            " minutes of cpu time.",
            "caught":"mask of the caught signals, see signal(7). According to the width "
            "of the field, a 32 or 64 bits mask in hexadecimal format is displayed.",
            "cgname":"display name of control groups to which the process belongs.",
            "cgroup":"display control groups to which the process belongs",
            "class":"scheduling class of the process.  (aliaspolicy, cls).  Field's possible"
            " values are:\n   -   not reported\n   TS  SCHED_OTHER\n   FF  SCHED_FIFO\n   RR"
            "  SCHED_RR\n   B   SCHED_BATCH\n   ISO SCHED_ISO \n   IDL SCHED_IDLE\n   DLN"
            " SCHED_DEADLINE \n   ?   unknown value",
            "comm":"command name (only the executable name). Modifications to the command"
            " name will not be shown.  A process marked <defunct> is partly dead, waiting to "
            "be fully destroyed by its parent.  The output in this column may contain spaces.",
            "cputime":"cumulative CPU time, \"[DD-]hh:mm:ss\" format.",
            "cputimes":"cumulative CPU time in seconds",
            "drs":"data resident set size, the amount of physical memory devoted to other than"
            " executable code.",
            "egid":"effective group ID number of the process as a decimal integer",
            "egroup":"effective group ID of the process. This will be the textual group ID, if"
            " it can be obtained and the field width permits, or a decimal representation "
            "otherwise.",
            "etime":"elapsed time since the process was started, in the form [[DD-]hh:]mm:ss.",
            "etimes":"elapsed time since the process was started, in seconds.",
            "euid":"effective user ID",
            "euser":"effective user name. This will be the textual user ID, if it can be "
            "obtained and the field width permits, or a decimal representation otherwise.",
            "fgid":"filesystem access group ID.",
            "fgroup":"filesystem access group ID. This will be the textual group ID, if it can "
            "be obtained and the field width permits, or a decimal representation otherwise.",
            "fuid":"filesystem access user ID",
            "fuser":"filesystem access user ID. This will be the textual user ID, if it can be "
            "obtained and the field width permits, or a decimal representation otherwise",
            "fname":"first 8 bytes of the base name of the process's executable file. The output"
            " in this column may contain spaces.",
            "gid":"effective group ID number of the process as a decimal integer.",
            "group":"effective group ID of the process. This will be the textual group ID, if it"
            " can be obtained and the field width permits, or a decimal representation otherwise.",
            "ignored":"mask of the ignored signals, see signal(7). According to the width of the "
            "field, a 32 or 64 bits mask in hexadecimal format is displayed.",
            "ni":"nice value. This ranges from 19 (nicest) to -20 (not nice to others), see nice(1).",
            "nlwp":"number of lwps (threads) in the process.",
            "ouid":"displays the Unix user identifier of the owner of the session of a process, if "
            "systemd support has been included",
            "pending":"mask of the pending signals. See signal(7). Signals pending on the process "
            "are distinct from signals pending on individual threads. Use the m option or the -m "
            "option to see both. According to the width of the field, a 32 or 64 bits mask in "
            "hexadecimal format is displayed",
            "pgid":"process group ID or, equivalently, the process ID of the process group leader",
            "pid":"a number representing the process ID",
            "ppid":"parent process ID.",
            "pri":"priority of the process. Higher number means lower priority.",
            "psr":"processor that process is currently assigned to.",
            "rgid":"real group ID.",
            "rgroup":"real group name. This will be the textual group ID, if it can be obtained and"
            " the field width permits, or a decimal representation otherwise",
            "rss":"resident set size, the non-swapped physical memory that a task has used "
            "(in kilobytes).",
            "rtprio":"realtime priority.",
            "ruid":"real user ID.",
            "ruser":"real user ID. This will be the textual user ID, if it can be obtained and the "
            "field width permits, or a decimal representation otherwise.",
            "sgid":"saved group ID.",
            "sgroup":"saved group name. This will be the textual group ID, if it can be obtained and"
            " the field width permits, or a decimal representation otherwise.",
            "suid":"saved user ID.",
            "suser":"saved user name. This will be the textual user ID, if it can be obtained and the"
            " field width permits, or a decimal representation otherwise.",
            "tt":"controlling tty (terminal).",
            "uid":"effective user ID",
            "user":"effective user name. This will be the textual user ID, if it can be "
            "obtained and the field width permits, or a decimal representation otherwise."
            }
def printHelp():
    print('SeguimentProcessos.py [-hv]')
    print('SeguimentProcessos.py [--helps --version]')
    print ('  opcions:')
    print ('    -h --help       print this mesage and exit')
    print ('    -v --version    print the version and exit ')
    
def main(pi,argv):
    try:
        opts,args  = getopt.getopt(argv,"hv",["help","version"])
    except getopt.GetoptError:
        printHelp()
        sys.exit(2)
    for opt,arg in opts:
        if opt in ('-h' , "--help"):
            printHelp()
            sys.exit()
        elif opt in ("-v", "--version"):
            print("SeguimentProcessos.py version: ", version)
            sys.exit()
    win = MaxCpuProcess(pid)
    if win.UP is None:
        exit(1)
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
    exit(0)
            
version = "0.5"

if __name__ == "__main__":
    pid = str(os.getpid())
    main(pid,sys.argv[1:])
