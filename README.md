Control of CPU occupation for processes.

Lately I have encountered processes that occupy 100% of the CPU by blocking the 
computer.

As far as I'm copying, these processes are mainly related to Chromium 
(also Vivaldi) and Thumbnails.so. In order to try to control it I have created 
this utility, which tracks the processes that occupy more CPUs, and those that 
exceed configure percent of employment are reducing the priority to 19. If even 
so, indicated occupation, kills the process, sending him a SIGKILL.

At the same time, it keeps a record of the processes that exceed this CPU 
limitation